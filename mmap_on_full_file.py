import mmap
import threading
import os
import test_tools


threads = [1, 3, 5, 10, 20, 100]
chunk_sizes = [256, 4 * 1024, 64 * 1024, 256 * 1024, 1024 * 1024, 10 * 1024 * 1024]


def read(m, start, end):
    i = start
    while i < end:
        _ = m[i: i+chunk_size]
        i += chunk_size


def read_with_mmap():
    with open('testfile', 'rb') as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as m:
            file_size = os.stat('testfile').st_size
            size_per_thread = int(file_size / n_threads)
            i = 0
            thread_pool = []

            for n in range(n_threads):
                thread = threading.Thread(target=read, args=(m, i, i + size_per_thread))
                thread.start()
                thread_pool.append(thread)
                i += size_per_thread

            for thread in thread_pool:
                thread.join()


if __name__ == '__main__':
    test_tools.Testfile.create_if_not_exists(size_in_gb=60)

    print('MMAP ON FULL FILE BENCHMARK')
    print()
    for i in chunk_sizes:
        chunk_size = i
        for j in threads:
            n_threads = j
            print()
            print(f'n threads: {n_threads}')
            print(f'chunk size: {chunk_size}')
            print(test_tools.RepeatedBenchmark(read_with_mmap, 3, True))
