import test_tools
import os
import threading


threads = [1, 3, 5, 10, 20, 100]
chunk_sizes = [256, 4 * 1024, 64 * 1024, 256 * 1024, 1024 * 1024, 10 * 1024 * 1024]


def read(start, end):
    with open('testfile', 'rb') as f:
        f.seek(start)
        i = start
        while i < end:
            i += chunk_size
            a = f.read(chunk_size)


def read_with_regular_io():
    file_size = os.stat('testfile').st_size
    size_per_thread = int(file_size / n_threads)
    i = 0
    thread_pool = []

    for n in range(n_threads):
        thread = threading.Thread(target=read, args=(i, i + size_per_thread))
        thread.start()
        thread_pool.append(thread)
        i += size_per_thread

    for thread in thread_pool:
        thread.join()


if __name__ == '__main__':
    test_tools.Testfile.create_if_not_exists(size_in_gb=60)

    print('REGULAR I/O BENCHMARK')
    print()
    for i in chunk_sizes:
        chunk_size = i
        for j in threads:
            n_threads = j
            print()
            print(f'n threads: {n_threads}')
            print(f'chunk size: {chunk_size}')
            print(test_tools.RepeatedBenchmark(read_with_regular_io, 1, True))
