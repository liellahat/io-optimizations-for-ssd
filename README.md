# IO optimizations for SSD

Lately I have been trying to figure out what is the fastest approach for reading a large file from an SSD.

The approaches that I've tested:

- `regular_io` - Reading the file using regular IO operatios, with a buffer (reading X bytes at a time).
- `mmap_on_full_file` - Mapping the whole file to memory with `mmap`, and reading small parts of the mapped file.
- `mmap_on_chunks` - Mapping a small part of the file to memory at a time, and reading the whole mapped part.

For each approach, I ran multiple tests with different buffer sizes and thread counts.

I measured the following parameters:
- Speed
- Average % cpu
- Average physical memory usage 
- Peak physical memory usage

The tests were all ran on the same 10GB file on my local machine.


#### Specs
- Windows 10
- intel i5 6600k
- 16 GB RAM
- Samsung SSD 850 EVO 250GB 


#### Notes 
- After each test, I allocated a random array of 12GB to memory in order to clean any cache/standby memory. 
In reality, some parts of the file were probably still cached after the 12GB allocation, because only 12 gigs were allocated out of 16.
I spared the last 4 gigs for safety reasons (didn't want my poor computer to die).
- Each test was ran 3 times while the computer was idle.
- Measurements were done for the whole machine and not for a specific process. I used the python module `psutil`.
- In the `mmap_on_chunks` test I had to use chunk sizes that were multiples of 64KB (`mmap` requires the offset parameter to be a multiple of 64K by default).


#### Results

Best time measurement for each approach is marked in blue.

![](images/results_table.png)


#### Conclusion
- Fastest results were observed with regular IO, 1MB buffer size, and 5 threads.
- Reading from the mmaped file was by far slower than regular IO and required much more physical memory - the whole 10GB file was loaded to RAM at once. 
- Memory mapping chunks of the file is extremely inefficient, but required much less physical memory. In reality this approach is completely useless because it avoids any optimizations that `mmap` offers. It's like doing regular IO, but much slower.


#### Farther research
- How is performance affected when the file is bigger than RAM?
- What is the best approach for implementing sliding window over the file?
