import test_tools
import os
import threading
import mmap
import math


threads = [1, 3, 5, 10, 20, 100]
chunk_sizes = [64 * 1024, 256 * 1024, 1024 * 1024, 10 * 1024 * 1024]


def read(start, end):
    with open('testfile', 'rb') as f:
        q = 0
        while q < end - start:
            try:
                if q + chunk_size > end - start:
                    length = end - start - q
                else:
                    length = chunk_size

                with mmap.mmap(f.fileno(), length=length, offset=start + q, access=mmap.ACCESS_READ) as m:
                    _ = m[:]
                    q += chunk_size
            except:
                print('Exception')
                print(start, start + q, start+q+chunk_size)
                return


def read_with_moving_mmap_view():
    file_size = os.stat('testfile').st_size
    n_batches = int(file_size / 64 / 1024)  # number of 64KB chunks in the file
    bathces_per_thread = int(n_batches / n_threads)
    size_per_thread = bathces_per_thread * 64 * 1024  # aligning the size per thread to 64KB
    q = 0
    thread_pool = []

    print(f'size per thread: {size_per_thread}')

    for n in range(n_threads - 1):
        thread = threading.Thread(target=read, args=(q, q + size_per_thread))
        thread.start()
        thread_pool.append(thread)
        q += size_per_thread

    thread = threading.Thread(target=read, args=(q, file_size))  # assuming file size is multiple of 64 * 1024
    thread.start()
    thread_pool.append(thread)
	
    for thread in thread_pool:
        thread.join()


if __name__ == '__main__':
    test_tools.Testfile.create_if_not_exists(size_in_gb=10)

    print('MMAP ON CHUNKS BENCHMARK')
    print()
    for i in chunk_sizes:
        chunk_size = i
        for j in threads:
            n_threads = j
            print()
            print(f'n threads: {n_threads}')
            print(f'chunk size: {chunk_size}')
            print(test_tools.RepeatedBenchmark(read_with_moving_mmap_view, 3, True))
