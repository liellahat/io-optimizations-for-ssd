import psutil
import time
import datetime
import threading
import os


class Testfile:

    @staticmethod
    def create(size_in_gb):
        with open('testfile', 'wb') as f:
            for i in range(size_in_gb):
                f.write(os.urandom(1024 * 1024 * 1024))

    @staticmethod
    def exists(size_in_gb = None):
        if size_in_gb is None:
            return os.path.isfile('testfile')

        return os.path.exists('testfile') and os.stat('testfile').st_size == size_in_gb * 1024 * 1024 * 1024

    @staticmethod
    def create_if_not_exists(size_in_gb):
        if not Testfile.exists(size_in_gb):
            Testfile.create(size_in_gb)


class ResourceChecker:

    def __init__(self):
        self._times_sampled = 0
        self._total_ram = 0
        self._total_commit = 0
        self._total_cpu = 0

        self.peak_ram = 0
        self.peak_commit = 0
        self.peak_cpu = 0

    def sample(self):
        self._times_sampled += 1

        ram = psutil.virtual_memory().used
        commit = psutil.swap_memory().used
        cpu_percent = psutil.cpu_percent()

        if ram > self.peak_ram:
            self.peak_ram = ram

        if commit > self.peak_commit:
            self.peak_commit = commit

        if cpu_percent > self.peak_cpu:
            self.peak_cpu = cpu_percent

        self._total_ram += ram
        self._total_commit += commit
        self._total_cpu += cpu_percent

    @property
    def avg_ram(self):
        return int(self._total_ram / self._times_sampled)

    @property
    def avg_commit(self):
        return int(self._total_commit / self._times_sampled)

    @property
    def avg_cpu(self):
        return int(self._total_cpu / self._times_sampled)


class Benchmark:

    def __init__(self, method):
        self._stop = False
        self.resources = ResourceChecker()

        benchmark_thread = threading.Thread(target=self.do_benchmark, daemon=True)
        benchmark_thread.start()

        start_time = datetime.datetime.now()
        method()
        self.time = (datetime.datetime.now() - start_time).total_seconds()

        self._stop = True
        benchmark_thread.join()

    def do_benchmark(self):
        while not self._stop:
            self.resources.sample()
            time.sleep(0.5)


class RepeatedBenchmark:

    def __init__(self, method, times, clean_standby_memory = None):
        self._times_tested = times
        self._benchmarks = []
        for i in range(times):
            self._benchmarks.append(Benchmark(method))
            if clean_standby_memory:
                self._clean_standby_memory()

    def _clean_standby_memory(self):
        # allocating 12 GB of random memory
        os.urandom(12 * 1024 * 1024 * 1024)

    @property
    def peak_cpu(self):
        return max([x.resources.peak_cpu for x in self._benchmarks])

    @property
    def peak_ram(self):
        return max([x.resources.peak_ram for x in self._benchmarks])

    @property
    def peak_commit(self):
        return max([x.resources.peak_commit for x in self._benchmarks])

    @property
    def avg_cpu(self):
        return sum([x.resources.avg_cpu for x in self._benchmarks]) / len(self._benchmarks)

    @property
    def avg_ram(self):
        return sum([x.resources.avg_ram for x in self._benchmarks]) / len(self._benchmarks)

    @property
    def avg_commit(self):
        return sum([x.resources.avg_commit for x in self._benchmarks]) / len(self._benchmarks)

    @property
    def avg_time(self):
        return sum([x.time for x in self._benchmarks]) / len(self._benchmarks)

    def __str__(self):
        return f'####### TEST RESULTS #######\n' \
               f'iterations: {self._times_tested}\n' \
               f'average time: {self.avg_time}s\n' \
               f'RESOURCE USAGE:\n' \
               f'CPU:\tpeak={int(self.peak_cpu)}%\tavg={int(self.avg_cpu)}%\n' \
               f'RAM:\tpeak={int(self.peak_ram / 1024 / 1024)}MB\tavg={int(self.avg_ram / 1024 / 1024)}MB\n' \
               f'COMMIT:\tpeak={int(self.peak_commit / 1024 / 1024)}MB\tavg={int(self.avg_commit / 1024 / 1024)}MB\n'
